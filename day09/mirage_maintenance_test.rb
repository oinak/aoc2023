# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative 'mirage_maintenance'

describe MirageMaintenance do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { MirageMaintenance.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      0 3 6 9 12 15
      1 3 6 10 15 21
      10 13 16 21 30 45
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(114)
    end
  end

  describe 'with advanced situation' do
    it 'returns the correct advanced answer' do
      _(subject.solution(advanced: true)).must_equal(2)
    end
  end
end
