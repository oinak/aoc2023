# frozen_string_literal: true

# Advent of code - Day 9
class MirageMaintenance
  attr_reader :data

  def initialize(filename)
    @data = File.readlines(filename).map { _1.split.map(&:to_i) }
  end

  def solution(advanced: false)
    advanced ? sum_extrapolated_precedents : sum_extrapolated_values
  end

  def sum_extrapolated_values
    data.map { |line| extrapolate_value(line) }.sum
  end

  def sum_extrapolated_precedents
    data.map { |line| extrapolate_value(line.reverse) }.sum
  end

  def extrapolate_value(numbers) = build_history(numbers).map(&:last).sum

  def build_history(numbers)
    [numbers].tap do |history|
      until history.last.all?(&:zero?)
        next_layer = history.last.each_cons(2).map { |a, b| b - a }
        history << next_layer
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = MirageMaintenance.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
