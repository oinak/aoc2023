# frozen_string_literal: true

# Advent of code - Day 10
class PipeMaze
  attr_reader :maze, :distances

  def initialize(filename)
    @maze = File.readlines(filename).map(&:strip)
    @distances = map_distances
  end

  def solution(advanced: false)
    advanced ? count_enclosed_tiles : furthest_spot
  end

  def furthest_spot = distances.values.max

  # given a loop, if something is inside, you will cross an even num of walls
  # to get to it
  # inspired from https://www.reddit.com/r/adventofcode/comments/18evyu9/comment/kcs2gnf/?context=3
  # rubocop:disable Metrics/MethodLength
  def count_enclosed_tiles
    maze.each_index.inject(0) do |inner_tiles, row|
      crosses = 0
      maze[row].chars.each_with_index do |tile, column|
        if in_loop?(row, column)
          crosses += croses_for_tile(tile)
        elsif crosses % 4 == 2
          inner_tiles += 1
        end
      end
      inner_tiles
    end
  end
  # rubocop:enable Metrics/MethodLength

  def in_loop?(row, column) = distances.key?([row, column])

  def croses_for_tile(tile)
    return 2 if tile == '|'    # bordes as whole cross
    return 1 if tile =~ /[L7]/ # corners as half cross
    return -1 if tile =~ /[FJ]/

    0 # crosses += 0 if tile == '-' # follow the line
  end

  def map_distances
    nodes = [start]
    nodes.each_with_object({ start => 0 }) do |node, distances|
      neighbor_coords(*node).each do |new_node|
        next if distances[new_node] # already visited

        distances[new_node] = distances[node] + 1
        nodes << new_node
      end
    end
  end

  def start
    @start ||= maze.each_with_index do |row, row_num|
      s_index = row.index('S')
      break [row_num, s_index] if s_index
    end
  end

  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/CyclomaticComplexity
  def neighbor_coords(row, col)
    case maze[row][col]
    when '|' then [north(row, col), south(row, col)]
    when '-' then [east(row, col), west(row, col)]
    when 'L' then [north(row, col), east(row, col)]
    when 'J' then [north(row, col), west(row, col)]
    when '7' then [south(row, col), west(row, col)]
    when 'F' then [south(row, col), east(row, col)]
    when 'S' then start_neighbors(row, col)
    else
      []
    end
  end
  # rubocop:enable Metrics/CyclomaticComplexity
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength

  def north(row, col) = [row - 1, col]
  def east(row, col) = [row, col + 1]
  def south(row, col) = [row + 1, col]
  def west(row, col) = [row, col - 1]

  # S adjacents that have S as connected neighbour
  def start_neighbors(row, col)
    nodes = %i[north east south west].map { |direction| send(direction, row, col) }
    nodes.select { neighbor_coords(*_1).include? [row, col] }
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = PipeMaze.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
