# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './pipe_maze'

describe PipeMaze do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { PipeMaze.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      ..F7.
      .FJ|.
    SJ.L7
    |F--J
    LJ...
      TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(8)
    end
  end

  describe 'with advanced situation' do
    describe 'on small map' do
      let(:lines) do
        <<~TXT
          ...........
          .S-------7.
          .|F-----7|.
          .||.....||.
          .||.....||.
          .|L-7.F-J|.
          .|..|.|..|.
          .L--J.L--J.
          ...........
        TXT
      end
      it 'returns the correct advanced answer' do
        _(subject.solution(advanced: true)).must_equal(4)
      end
    end

    describe 'on large map' do
      let(:lines) do
        <<~TXT
          .F----7F7F7F7F-7....
          .|F--7||||||||FJ....
          .||.FJ||||||||L7....
          FJL7L7LJLJ||LJ.L-7..
          L--J.L7...LJS7F-7L7.
          ....F-J..F7FJ|L7L7L7
          ....L7.F7||L7|.L7L7|
          .....|FJLJ|FJ|F7|.LJ
          ....FJL-7.||.||||...
          ....L---J.LJ.LJLJ...
        TXT
      end
      it 'returns the correct advanced answer' do
        _(subject.solution(advanced: true)).must_equal(8)
      end
    end
  end
end
