# frozen_string_literal: true

# Advent of code - Day 1
class Trebuchet
  attr_reader :data

  def initialize(filename) = @data = File.readlines(filename).map(&:strip)

  def solution(advanced: false) = advanced ? sum_with_words : sum_of_digits

  def sum_of_digits
    data.reduce(0) do |sum, line|
      digits = line.chars.grep(/\d/).map(&:to_i)
      sum + "#{digits.first}#{digits.last}".to_i
    end
  end

  WORDS = %w[one two three four five six seven eight nine].freeze
  def anti_regex = /(#{WORDS.map(&:reverse).join('|')}|\d)/
  def regex = /(#{WORDS.join('|')}|\d)/

  def sum_with_words
    data.reduce(0) do |sum, line|
      digits = line_digits(line)
      reverse_digits = reverse_line_digits(line)
      first = digit_value(digits.first)
      last = digit_value(reverse_digits.first)

      value = "#{first}#{last}".to_i
      sum + value
    end
  end

  def line_digits(line) = line.scan(regex).flatten
  def reverse_line_digits(line) = line.reverse.scan(anti_regex).flatten.map(&:reverse)
  def digit_value(digit) = ([0] + WORDS).index(digit) || digit.to_i
end

if __FILE__ == $PROGRAM_NAME
  obj = Trebuchet.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
