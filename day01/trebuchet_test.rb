# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative 'trebuchet'

describe Trebuchet do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { Trebuchet.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
    TXT
  end

  describe 'with basic situation' do
    it 'returns the sum of digits' do
      _(subject.solution).must_equal(142)
    end
  end

  describe 'with advanced situation' do
    let(:lines) do
      <<~TXT
        two1nine
        eightwothree
        abcone2threexyz
        xtwone3four
        4nineeightseven2
        zoneight234
        7pqrstsixteen
      TXT
    end

    it 'returns the sum of words and normal digits' do
      _(subject.solution(advanced: true)).must_equal(281)
    end

    describe 'with overlapping word digits' do
      let(:lines) do
        <<~TXT
          oneight
          eightwo
          twone
        TXT
      end

      it 'returns the correct digits' do
        _(subject.solution(advanced: true)).must_equal(18 + 82 + 21)
      end
    end
  end
end
