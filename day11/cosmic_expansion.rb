# frozen_string_literal: true

# Advent of code - Day 11
class CosmicExpansion
  attr_reader :data

  def initialize(filename)
    @data = File.readlines(filename).map { _1.strip.chars }
  end

  def solution(advanced: false)
    if advanced
      sum_of_expanded_distances(999_999)
    else
      sum_of_expanded_distances(1) # part 2 generalized to cover part 1
    end
  end

  def sum_of_expanded_distances(factor)
    expand_coords_by_factor(factor:).then { |expanded| sum_of_distances(expanded) }
  end

  private

  X = 0
  Y = 1
  BLANK = '.'
  GALAXY = '#'

  def expand_coords_by_factor(factor:)
    pre_expanded = galaxies.map(&:clone)
    expanded_y = expand_axis(
      star_data: data, expanded: pre_expanded, coord: Y, factor:
    )
    expand_axis(
      star_data: data.transpose, expanded: expanded_y, coord: X, factor:
    )
  end

  def expand_axis(star_data:, coord:, expanded:, factor:)
    star_data.each_with_index.with_object(expanded) do |(line, axis_value), output|
      if line.all? { |tile| blank?(tile) }
        galaxies.each_with_index do |original, g_index|
          output[g_index][coord] += factor if original[coord] > axis_value
        end
      end
    end
  end

  def sum_of_distances(expanded_galaxies)
    expanded_galaxies.each_with_object({}) do |galaxy, distances|
      expanded_galaxies.each do |other|
        next if other == galaxy

        key = [galaxy, other].sort
        next if distances.key? key

        distances[key] = distance(galaxy, other)
      end
    end.values.sum
  end

  def distance(one, other)
    x0, y0 = one
    x1, y1 = other
    (x1 - x0).abs + (y1 - y0).abs
  end

  def galaxies
    @galaxies ||=
      data.each_with_index.with_object([]) do |(row, y), coords|
        row.each.with_index { |tile, x| coords << [x, y] if galaxy? tile }
      end
  end

  def blank?(char) = char == BLANK
  def galaxy?(char) = char == GALAXY
end

if __FILE__ == $PROGRAM_NAME
  obj = CosmicExpansion.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
