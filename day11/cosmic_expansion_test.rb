# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './cosmic_expansion'

describe CosmicExpansion do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { CosmicExpansion.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      ...#......
      .......#..
      #.........
      ..........
      ......#...
      .#........
      .........#
      ..........
      .......#..
      #...#.....
    TXT
  end

  let(:expanded) do
    <<~TXT
      ....#........
      .........#...
      #............
      .............
      .............
      ........#....
      .#...........
      ............#
      .............
      .............
      .........#...
      #....#.......
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct sum of galaxy distances' do
      _(subject.solution).must_equal(374)
    end
  end

  describe 'with small cosmic expansion' do
    it 'returns the correct advanced answer' do
      _(subject.sum_of_expanded_distances(9)).must_equal(1030)
    end
  end

  describe 'with medium cosmic expansion' do
    it 'returns the correct advanced answer' do
      _(subject.sum_of_expanded_distances(99)).must_equal(8410)
    end
  end
end
