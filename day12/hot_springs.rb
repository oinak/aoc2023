# frozen_string_literal: true

# logic inspired by https://cutonbuminband.github.io/AOC/qmd/2023.html

# Advent of code - Day 12
class HotSprings
  attr_reader :data

  def initialize(filename)
    @data = File.readlines(filename).map(&:strip)
    @cache = {}
  end

  def solution(advanced: false) = advanced ? sum_unfolded(5) : sum_unfolded(1)
  def sum_unfolded(folds) = data.sum { |line| count(*parse_unfolded(line, folds)) }

  def parse_unfolded(line, folds = 1)
    str, numbers = line.split
    unfolded_str = ([str] * folds).join('?')
    [unfolded_str.chars, numbers.split(',').map(&:to_i) * folds]
  end

  def cached_count(springs, lengths) = @cache[[springs, lengths]] ||= count(springs, lengths)

  ON = '#'
  MAYBE = '?'
  OFF = '.'

  def count(springs, lengths)
    total_length = lengths.sum
    return 0 if total_length < springs.count(ON)
    return 0 if total_length > springs.count { [ON, MAYBE].include? _1 }
    return 1 if total_length == 0

    case springs[0]
    when OFF   then cached_count(springs[1..], lengths)
    when ON    then count_on(springs, lengths)
    when MAYBE then count_maybe(springs, lengths)
    end
  end

  def count_on(springs, lengths)
    first_length, *rest_lengths = lengths
    return 0 unless match_beginning(springs, first_length)
    return 1 if first_length == springs.length # block covers all remaining springs

    cached_count(springs[first_length + 1..], rest_lengths)
  end

  def count_maybe(springs, lengths)
    off_branch = cached_count(springs[1..], lengths)
    on_branch = cached_count([ON] + springs[1..], lengths)

    off_branch + on_branch
  end

  def match_beginning(springs, length)
    all_prefix_maybe = springs[...length].all? { [ON, MAYBE].include? _1 }
    uses_all_springs = springs.length == length
    block_ends_there = [MAYBE, OFF].include? springs[length]

    all_prefix_maybe && (uses_all_springs || block_ends_there)
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = HotSprings.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
