# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative 'hot_springs'

describe HotSprings do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { HotSprings.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      ???.### 1,1,3
      .??..??...?##. 1,1,3
      ?#?#?#?#?#?#?#? 1,3,1,6
      ????.#...#... 4,1,1
      ????.######..#####. 1,6,5
      ?###???????? 3,2,1
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(21)
    end
  end

  describe 'with advanced situation' do
    it 'returns the correct advanced answer' do
      _(subject.solution(advanced: true)).must_equal(525_152)
    end
  end
end
