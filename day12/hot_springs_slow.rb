# frozen_string_literal: true

# Advent of code - Day 12
class HotSprings
  attr_reader :data

  def initialize(filename)
    @data = File.readlines(filename).map(&:strip)
  end

  def solution(advanced: false)
    advanced ? 0 : sum_arrangements
  end

  def sum_arrangements
    data.map { |line| num_arrangements(line) }.sum
  end

  def num_arrangements(line)
    str, numbers = line.split
    arrangements(str, numbers.split(',').map(&:to_i))
  end

  def arrangements(chars, numbers)
    damaged = chars.index('?')
    return numbers_for(chars) == numbers ? 1 : 0 unless damaged

    arrangements("#{chars[...damaged]}##{chars[(damaged + 1)..]}", numbers) +
      arrangements("#{chars[...damaged]}.#{chars[(damaged + 1)..]}", numbers)
  end

  def numbers_for(str)
    chunks = str.chars.chunk { _1 == '#' }.to_a
    chunks.select(&:first).map { _1.last.size }
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = HotSprings.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
