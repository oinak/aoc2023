# frozen_string_literal: true

# Advent of code - Day 7
class CamelCards
  attr_reader :hands, :advanced

  def initialize(filename)
    @hands = File.readlines(filename).map(&:strip).map do |line|
      cards, bid = line.split
      { cards:, bid: bid.to_i }
    end
  end

  def solution(advanced: false)
    @advanced = advanced
    ranked_hands.map { |h| h[:bid] * h[:rank] }.sum
  end

  def ranked_hands
    parsed_hands = hands.map do |hand|
      hand.merge(
        value: hand_value(hand[:cards]), 
        count_cards: count_cards_with_joker(hand[:cards])
      )
    end

    parsed_ranked_hands = parsed_hands.sort_by { |h| h[:value] }.map.with_index(1) { |h, i| h.merge(rank: i) }

    display(parsed_ranked_hands)

    parsed_ranked_hands
  end
  # {:cards=>"32T3K", :bid=>765, :value=>[2, 3, 2, 10, 3, 13],    :rank=>1}
  # {:cards=>"KTJJT", :bid=>220, :value=>[3, 13, 10, 11, 11, 10], :rank=>2}
  # {:cards=>"KK677", :bid=>28,  :value=>[3, 13, 13, 6, 7, 7],    :rank=>3}
  # {:cards=>"T55J5", :bid=>684, :value=>[4, 10, 5, 5, 11, 5],    :rank=>4}
  # {:cards=>"QQQJA", :bid=>483, :value=>[4, 12, 12, 12, 11, 14], :rank=>5}

  PLAY_VALUES = {
    [5] => 7,            # five of a kind
    [1, 4] => 6,         # four of a kind
    [2, 3] => 5,         # full house
    [1, 1, 3] => 4,      # three of a kind
    [1, 2, 2] => 3,      # two pairs
    [1, 1, 1, 2] => 2,   # pair
    [1, 1, 1, 1, 1] => 1 # highest card
  }.freeze

  def hand_value(cards)
    [PLAY_VALUES[count_cards_with_joker(cards)]] + cards.chars.map { |c| card_values[c] }
  end

  SPECIAL_CARDS = { 'A' => 14, 'K' => 13, 'Q' => 12, 'J' => 11, 'T' => 10 }.freeze

  def card_values
    @card_values ||= {}.merge(special_cards).tap do |h|
      h.default_proc = ->(_, key) { key.to_i }
    end
  end

  def count_cards(cards) = cards.chars.tally.values.sort

  ## PART 2
  def special_cards = advanced ? SPECIAL_CARDS.merge('J' => 1) : SPECIAL_CARDS

  def count_cards_with_joker(cards)
    counts = count_cards(cards)
    return counts unless advanced

    num_js = cards.scan('J').size
    return counts if [0, 5].include?(num_js) # no changes possible

    counts_js = count_cards(cards.tr('J', ''))
    counts_js.tap { |cj| cj[-1] += num_js }
  end

  def display(hands)
    return unless ENV.fetch('DEBUG', false)

    hands.each do |h|
      v = h[:value].map { format('%2d', _1) }.join(' ')
      c = h[:count_cards].map { format('%2d', _1) }.join(' ')
      puts("#{h[:cards].chars.join(' ')} | #{v} | #{c}")
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = CamelCards.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
