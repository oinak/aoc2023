# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './camel_cards'

describe CamelCards do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { CamelCards.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      32T3K 765
      T55J5 684
      KK677 28
      KTJJT 220
      QQQJA 483
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(6440)
    end
  end

  describe 'with advanced situation' do
    it 'returns the correct advanced answer' do
      _(subject.solution(advanced: true)).must_equal(5905)
    end
  end
end
