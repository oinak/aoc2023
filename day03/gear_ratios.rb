# frozen_string_literal: true

# Advent of code - Day 3
class GearRatios
  attr_accessor :data, :numbers, :symbols

  def initialize(filename)
    @data = File.readlines(filename).map(&:strip)
    @numbers = []
    @symbols = {}
    read_input
    find_parts
  end

  def solution(advanced: false) = advanced ? sum_of_gear_ratios : sum_of_parts

  def sum_of_parts = parts.map { _1[:chars].to_i }.sum

  def parts = @numbers.select { _1[:part] }

  def find_parts
    symbols.each_key do |row_col|
      part_numbers_for(row_col).each do |part_number|
        part_number.merge!(part: true)
      end
    end
    display
  end

  def part_numbers_for(row_col)
    numbers.select { |n| !n[:part] && n[:coords].intersect?(box_around(*row_col)) }
  end

  # Determines a box around a coord
  def box_around(row, col)
    r0 = [row - 1, 0].max
    r1 = [row + 1, height - 1].min
    c0 = [col - 1, 0].max
    c1 = [col + 1, width - 1].min
    (r0..r1).to_a.product((c0..c1).to_a).reject { _1 == [row, col] }
  end

  def read_input
    (0...height).each do |row|
      @in_number = false # if line ends in number it does not change inside
      (0...width).each { |col| read_number(row, col, data[row][col]) }
    end
  end

  def height = data.size
  def width = data.first.size

  def read_number(row, col, current_char)
    if current_char =~ /\d/
      if @in_number
        continue_number(current_char, row:, col:)
      else
        new_number(current_char, row:, col:)
      end
    elsif current_char != '.'
      symbols[[row, col]] = current_char
    end
    @in_number = current_char =~ /\d/
  end

  def new_number(current_char, row:, col:)
    numbers.append({ chars: current_char, coords: [[row, col]] })
  end

  def continue_number(current_char, row:, col:)
    numbers.last[:chars] << current_char
    numbers.last[:coords] << [row, col]
  end

  ## PART 2
  GEAR = '*'
  def sum_of_gear_ratios
    gears.each_with_object([]) do |(row, col), ratios|
      found = gear_parts(row, col)
      ratios << (found[0][:chars].to_i * found[1][:chars].to_i) if found.size == 2
    end.sum
  end

  def gears = symbols.select { |_, char| char == GEAR }.keys

  def gear_parts(row, col)
    numbers.select { |n| n[:coords].intersect?(box_around(row, col)) }
  end

  ## DEBUGING ONLY:

  def display
    return unless ENV['DEBUG']

    (0...height).each do |r|
      print "\n#{format '%4d', r}|"
      (0...width).each { |c| print_char(r, c) }
      print '|'
    end
    puts "\n---"
  end

  def print_char(row, col) # rubocop:disable Metrics/AbcSize
    char = data[row][col]
    if symbols.keys.include?([row, col]) then print green(char)
    elsif symbols.keys.any? { box_around(*_1).include?([row, col]) } then print yellow(char)
    elsif parts.find { _1[:coords].include?([row, col]) } then print blue(char)
    elsif char =~ /\d/ then print red(char) # but not in parts
    else
      print(' ')
    end
  end

  def red(text)= "\e[0;31m#{text}\e[0m"
  def green(text)= "\e[0;32m#{text}\e[0m"
  def yellow(text)= "\e[0;33m#{text}\e[0m"
  def blue(text)= "\e[0;34m#{text}\e[0m"
  def magenta(text)= "\e[0;35m#{text}\e[0m"
end

if __FILE__ == $PROGRAM_NAME
  obj = GearRatios.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
