# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './gear_ratios'

describe GearRatios do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { GearRatios.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      467..114..
      ...*......
      ..35..633.
      ......#...
      617*......
      .....+.58.
      ..592.....
      ......755.
      ...$.*....
      .664.598..
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(4361)
    end
  end

  describe 'with advanced situation' do
    it 'returns the sum of the gear ratios' do
      _(subject.solution(advanced: true)).must_equal(467835)
    end
  end
end
