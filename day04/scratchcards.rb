# frozen_string_literal: true

# Advent of code - Day 4
class Scratchcards
  attr_reader :data, :cards

  def initialize(filename)
    @data = File.readlines(filename).map(&:strip)
    @cards = read_cards
  end

  def solution(advanced: false) = advanced ? procreated_scratchcards : sum_of_card_values

  def sum_of_card_values = cards.map { _1[:points] }.sum

  # Count the number of cards after applying all the copies from previous cards
  def procreated_scratchcards
    cards.each.with_index.inject(0) do |sum, (card, index)|
      won_cards = cards[index + 1, card[:wins]] # the cards tou win
      won_cards.each { |c| c[:copies] += card[:copies] }
      sum + card[:copies]
    end
  end

  # process each input line, and extrat a hash representation of a card
  def read_cards
    data.map do |line|
      wins = line_wins(line)
      { wins:, points: points(wins), copies: 1 }
    end
  end

  # (1 << (wins - 1)) works too
  def points(wins) = (2**(wins - 1)).to_i

  # Extract the number of winning_numbers on the playing side
  # Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
  def line_wins(line)
    number_lists = line.split(':').last.split('|')
    winning_numbers, playing_numbers = number_lists.map(&:split)
    (winning_numbers & playing_numbers).size
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = Scratchcards.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
