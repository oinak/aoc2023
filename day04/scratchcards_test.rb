# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './scratchcards'

describe Scratchcards do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { Scratchcards.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
      Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
      Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
      Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
      Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
      Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
    TXT
  end

  describe 'with basic situation' do
    it 'returns the sum of card values' do
      _(subject.solution).must_equal(13)
    end
  end

  describe 'with advanced situation' do
    it 'returns the number of procreated scratchcards' do
      _(subject.solution(advanced: true)).must_equal(30)
    end
  end
end
