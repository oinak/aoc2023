# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative 'haunted_wasteland'

describe HauntedWasteland do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { HauntedWasteland.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      RL

      AAA = (BBB, CCC)
      BBB = (DDD, EEE)
      CCC = (ZZZ, GGG)
      DDD = (DDD, DDD)
      EEE = (EEE, EEE)
      GGG = (GGG, GGG)
      ZZZ = (ZZZ, ZZZ)
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(2)
    end
  end

  describe 'with advanced situation' do
    let(:lines) do
      <<~TXT
        LR

        11A = (11B, XXX)
        11B = (XXX, 11Z)
        11Z = (11B, XXX)
        22A = (22B, XXX)
        22B = (22C, 22C)
        22C = (22Z, 22Z)
        22Z = (22B, 22B)
        XXX = (XXX, XXX)
      TXT
    end
    it 'returns the correct advanced answer' do
      _(subject.solution(advanced: true)).must_equal(6)
    end
  end
end
