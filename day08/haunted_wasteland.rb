# frozen_string_literal: true

# Advent of code - Day 8
class HauntedWasteland
  attr_reader :data

  def initialize(filename)
    @data = File.readlines(filename).map(&:strip)
  end

  def solution(advanced: false)
    advanced ? count_paralel_steps : count_steps
  end

  def count_steps
    read_map
    node = 'AAA'
    steps_count = 0
    while node != 'ZZZ'
      next_step = STEPS[@steps.next]
      node = @nodes[node][next_step]
      steps_count += 1
    end
    steps_count
  end

  # def count_paralel_steps
  #   read_map
  #   current_nodes = @nodes.keys.select { |key| key.end_with? 'A' }
  #   steps_count = 0
  #   until current_nodes.all? { |key| key.end_with? 'Z' }
  #     next_step = STEPS[@steps.next]
  #     current_nodes = current_nodes.map { |node| @nodes[node][next_step] }
  #     steps_count += 1
  #     puts steps_count if steps_count % 1000 == 0
  #   end
  #   steps_count
  # end
  def count_paralel_steps
    read_map
    start_nodes = @nodes.keys.select { |key| key.end_with? 'A' }
    counts = start_nodes.map do |start_node|
      @steps = data[0].chars.cycle # reset
      count_steps_from(start_node)
    end
    counts.inject(1) {|i, lcm| lcm.lcm(i) }
  end

  def count_steps_from(start_node)
    node = start_node 
    steps_count = 0
    until node.end_with? 'Z'
      next_step = STEPS[@steps.next]
      node = @nodes[node][next_step]
      steps_count += 1
    end
    steps_count
  end

  NODE_RE = /(?<node>\w{3}) = \((?<left>\w{3}), (?<right>\w{3})\)/
  STEPS = { 'L' => 0, 'R' => 1 }.freeze # step name to node array position
  def read_map
    @steps = data[0].chars.cycle
    @nodes = data[2..].inject({}) do |nodes, line|
      md = NODE_RE.match(line)
      md ? nodes.merge(md[:node] => [md[:left], md[:right]]) : nodes
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = HauntedWasteland.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
