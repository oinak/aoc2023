# frozen_string_literal: true

# Advent of code - Day 6
class WaitForIt
  attr_reader :data, :times, :records

  def initialize(filename)
    @data = File.readlines(filename).map(&:strip)
  end

  def solution(advanced: false)
    advanced ? single_race : product_of_win_options
  end

  ## Part 1

  def product_of_win_options
    times = data[0].scan(/\d+/).map(&:to_i)
    records = data[1].scan(/\d+/).map(&:to_i)
    ways_to_win = times.zip(records).map do |time, record|
      win_options(time, record)
    end
    ways_to_win.reduce(:*)
  end

  ## Part 2

  def single_race
    time = data[0].scan(/\d/).join.to_i
    record = data[1].scan(/\d/).join.to_i
    win_options(time, record)
  end

  ## Common

  def win_options(time, record)
    # distances = (1...time).map do |hold|
    #   travel_time = time - hold
    #   speed = hold
    #   travel_time * speed
    # end
    # wins = distances.select { |distance| distance > record }
    # wins.count
    (1...time).inject(0) do |count, hold|
      count + (((time - hold) * hold) > record ? 1 : 0)
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = WaitForIt.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
