# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './wait_for_it'

describe WaitForIt do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { WaitForIt.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      Time:      7  15   30
      Distance:  9  40  200
    TXT
  end

  describe 'with basic situation' do
    it 'returns the product of winning values' do
      _(subject.solution).must_equal(288)
    end
  end

  describe 'with advanced situation' do
    it 'returns the correct advanced answer' do
      _(subject.solution(advanced: true)).must_equal(71503)
    end
  end
end
