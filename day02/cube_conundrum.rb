# frozen_string_literal: true

# Advent of code - Day 2
class CubeConundrum
  attr_accessor :data

  def initialize(filename)
    @data = File.readlines(filename).map(&:strip)
  end

  def solution(advanced: false)
    advanced ? sum_minimum_powers : sum_possible_games
  end

  CUBES = { 'red' => 12, 'green' => 13, 'blue' => 14 }.freeze

  def sum_possible_games
    games.select { |_, g| possible?(g) }.keys.sum
  end

  def possible?(plays)
    plays.all? do |play|
      play.all? { |color, number| number <= CUBES[color] }
    end
  end

  # Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
  def games
    data.each_with_object({}) do |line, h|
      id_txt, plays_txt = line.split(':')
      h[id_txt.split.last.to_i] = line_plays(plays_txt)
    end
  end

  # 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
  def line_plays(plays_txt)
    plays_txt.split(';').map { |play_txt| line_play(play_txt) }
  end

  # 2 green, 6 blue
  def line_play(play_txt)
    play_txt.split(',').inject({}) do |h, color_txt|
      number, color = color_txt.split.map(&:strip)
      h.merge(color => number.to_i)
    end
  end

  ## PART 2
  def sum_minimum_powers
    games.values.map { |game| minimum_cubes(game).reduce(:*) }.sum
  end

  def minimum_cubes(plays)
    plays.each_with_object(Hash.new(0)) do |play, h|
      play.each do |color, number|
        h[color] = [h[color], number].max
      end
    end.values
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = CubeConundrum.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
