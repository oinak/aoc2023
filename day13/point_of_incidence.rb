# frozen_string_literal: true

# Advent of code - Day 13
class PointOfIncidence
  attr_reader :data

  def initialize(filename)
    @data = File.read(filename).split("\n\n").map { _1.split.map(&:chars) }
  end

  def solution(advanced: false) = advanced ? 0 : reflections

  def reflections
    data.sum do |group|
      group_reflections(group) + (100 * group_reflections(group.transpose))
    end
  end

  def group_reflections(group)
    group_inflection(group).then { |i| i.any? ? i[0] : 0 }
  end

  # a group only has a reflection if all its lines have the same symmetry
  def group_inflection(group)
    group.map { |line| line_inflection(line) }.reduce(&:intersection)
  end

  # return an array of the indexes where the line has local symmetry
  def line_inflection(line)
    l = line.length
    (1...l).select do |x|
      min = [x, l - x].min
      left = line[(x - min), min]
      right = line[x, min]
      left == right.reverse
    end
  end
end


if __FILE__ == $PROGRAM_NAME
  obj = PointOfIncidence.new(ARGV[0])
  puts obj.solution
  # puts obj.solution(advanced: true)
end
