# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './point_of_incidence'

describe PointOfIncidence do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { PointOfIncidence.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      #.##..##.
      ..#.##.#.
      ##......#
      ##......#
      ..#.##.#.
      ..##..##.
      #.#.##.#.

      #...##..#
      #....#..#
      ..##..###
      #####.##.
      #####.##.
      ..##..###
      #....#..#
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(405)
    end
  end

  describe 'with advanced situation' do
    it 'returns the correct advanced answer' do
      _(subject.solution(advanced: true)).must_equal('<NEW SOLUTION>')
    end
  end
end
