# frozen_string_literal: true
require 'debug'
require 'progressbar'
require 'yaml'

# Advent of code - Day 5
class SeedFertilizer
  attr_reader :data

  def initialize(filename)
    @data = File.read(filename).split("\n\n")
    @maps = read_maps.tap{ print _1 }
    @seen_seeds = []
  end

  def solution(advanced: false)
    if advanced
      seed_ranges_locations_min
    else
      seed_numbers.map { |seed| find_location('seed', seed) }.min
    end
  end

  def seed_ranges_locations_min
    ranges = seed_ranges
    seed_range_locations('seed', ranges[0])
  end

  def seed_range_locations(type, range)
    step = @maps[type]
    next_type = step[:to] if step
    locations = []
    pending = range
    puts "\n type:#{type}->#{next_type}"
    step[:lines].each do |source, destination|
      break unless pending
      rc = range_collision(pending, source)
      if rc[:ready]
        return destination.min if type == 'location'
        locations.concat(seed_range_locations(step[:to], rc[:ready]))
      end
      if rc[:pending]
        pending = rc[:pending]
      end
    end
    locations.concat(seed_range_locations(step[:to], pending))
    locations
  end
  
  #  overlap situation           pending ready
  # f0-f1 t0---------t1       => f0-f1 
  # f0----t0======f1-t1       => f0-t0   t0-f1
  #       t0---f0=f1-t1       =>         f0-f1
  #       t0---f0====t1-f1    => t1-f1   f0-t1
  #       t0---------t1 f0-f1 => f0-f1 
  def range_collision(from, to)
    puts "----> collision #{from} #{to}"
    if from.max < to.min                                    # f0-f1 t0---------t1       => f0-f1 
      { pending: from, ready: nil }
    elsif from.min < to.min && to.include?(from.max)        # f0----t0======f1-t1       => f0-t0   t0-f1
      { pending: [from.min...to.min], ready: [to.min..from.max] }
    elsif to.cover? from                                    #       t0---f0=f1-t1       =>         f0-f1
      { pending: nil, ready: from }
    elsif to.include?(from.min) && to.max < from.max        #       t0---f0====t1-f1    => t1-f1   f0-t1
      { pending: (to.max + 1)..from.min, ready: from.min..to.max }
    else                                                    #       t0---------t1 f0-f1 => f0-f1 
      { pending: from, ready: nil }
    end.tap { |o| puts "------> #{o}" }
  end

  def seed_numbers
    data[0].scan(/\d+/).map(&:to_i).tap { |o| puts "seed_numbers: #{o.size}" }
  end

  def read_maps
    data[1..].each_with_object({}) do |map_data, maps|
      name, *lines = map_data.split("\n")
      md = /(?<from>\w+)-to-(?<to>\w+) map:/.match(name)
      maps.merge!(md[:from] => { to: md[:to], lines: map_lines(lines)})
    end
  end

  def map_lines(lines)
    lines.each_with_object({}) do |line, ranges|
      dest_start, source_start, length = line.split.map(&:to_i)
      ranges[source_start..(source_start + length)] = dest_start
    end
  end

  def find_location(type, value)
    step = @maps[type]
    next_type = step[:to]
    src, dst = step[:lines].detect { |source, _| source.include?(value) }

    next_value = src ? dst + (value - src.min) : value
    return next_value if next_type == 'location'

    find_location(next_type, next_value)
  end

  def seed_ranges
    line_data = data[0].scan(/\d+/).map(&:to_i)
    range_data = line_data.each_slice(2).map { |start, length| (start..(start + length)) }
    range_data # .map(&:to_a).flatten.tap { |o| puts "seed_numbers: #{o.size}"}
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = SeedFertilizer.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
