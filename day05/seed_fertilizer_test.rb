# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './seed_fertilizer'

describe SeedFertilizer do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { SeedFertilizer.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      seeds: 79 14 55 13

      seed-to-soil map:
      50 98 2
      52 50 48

      soil-to-fertilizer map:
      0 15 37
      37 52 2
      39 0 15

      fertilizer-to-water map:
      49 53 8
      0 11 42
      42 0 7
      57 7 4

      water-to-light map:
      88 18 7
      18 25 70

      light-to-temperature map:
      45 77 23
      81 45 19
      68 64 13

      temperature-to-humidity map:
      0 69 1
      1 0 69

      humidity-to-location map:
      60 56 37
      56 93 4
    TXT
  end

  describe 'with basic situation' do
    it 'returns the lowest location for initial seeds' do
      _(subject.solution).must_equal(35)
    end
  end

  describe 'with advanced situation' do
    it 'returns the lowest location to any seeds' do
      _(subject.solution(advanced: true)).must_equal(46)
    end
  end
end
